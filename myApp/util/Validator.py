import re

VALID_IP_REG_EXP = r"^(25[0-4]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])( ?\. ?(25[0-4]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])){3}$"

VALID_MAC_REG_EXP = r"^([0-9a-fA-F]{2}([:-]?|$)){6}$|([0-9a-fA-F]{4}([.]|$)){3}"


def is_ip(ip):
    return re.match(VALID_IP_REG_EXP, ip) is not None


def is_mac(mac):
    return re.match(VALID_MAC_REG_EXP, mac) is not None


