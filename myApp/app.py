from flask import Flask, abort, make_response, jsonify
from flaskext.mysql import MySQL

from myApp.ShellFuncs import ping, traceroute
from myApp.MySqlWorker import MySqlWorker
from myApp.util.Validator import is_ip, is_mac
from config import mysqlConfig

app = Flask(__name__)

mysql = MySQL()
db_worker = MySqlWorker(mysql)

app.config['MYSQL_DATABASE_USER'] = mysqlConfig['MYSQL_DATABASE_USER']
app.config['MYSQL_DATABASE_PASSWORD'] = mysqlConfig['MYSQL_DATABASE_PASSWORD']
app.config['MYSQL_DATABASE_DB'] = mysqlConfig['MYSQL_DATABASE_DB']
app.config['MYSQL_DATABASE_HOST'] = mysqlConfig['MYSQL_DATABASE_HOST']
mysql.init_app(app)


@app.route("/api/<string:ip_or_mac>")
def get_full_information(ip_or_mac):
    if is_ip(ip_or_mac):
        data = db_worker.fetch_data_by_ip(ip_or_mac)
        return jsonify(data)

    elif is_mac(ip_or_mac):
        data = db_worker.fetch_data_by_mac(ip_or_mac)
        return jsonify(data)

    else:
        abort(404)


@app.route("/api/<string:ip_or_mac>/ping")
def do_ping(ip_or_mac):

    if is_ip(ip_or_mac):
        return jsonify(ping(ip_or_mac))

    elif is_mac(ip_or_mac):
        ip = db_worker.get_ip(ip_or_mac)

        if ip is None:
            abort(404)
        return jsonify(ping(ip))
    else:
        abort(404)


@app.route("/api/<string:ip_or_mac>/traceroute")
def do_traceroute(ip_or_mac):

    if is_ip(ip_or_mac):
        return jsonify(traceroute(ip_or_mac))

    elif is_mac(ip_or_mac):
        ip = db_worker.get_ip(ip_or_mac)

        if ip is None:
            abort(404)
        return jsonify(traceroute(ip))
    else:
        abort(404)


@app.errorhandler(Exception)
def error_handler(exception):
    abort(500)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.errorhandler(500)
def server_error(error):
    return make_response(jsonify({'error': 'Server entire error'}), 500)


if __name__ == '__main__':
    app.run()
