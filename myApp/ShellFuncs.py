import pexpect


def ping(ip):
    if len(ip) == 0:
        raise ValueError("ip is empty")
    return {"result": pexpect.run("ping -c 4 " + ip).decode("utf-8")}


def traceroute(ip):
    if len(ip) == 0:
        raise ValueError("ip is empty")
    return {"result": pexpect.run("traceroute " + ip).decode("utf-8")}

