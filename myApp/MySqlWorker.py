class MySqlWorker:
    def __init__(self, mysql):
        self.mysql = mysql

    def fetch_data_by_ip(self, ip):
        if len(ip) == 0:
            raise ValueError("ip can't be empty")
        return parse_data(self.fetch_data("SELECT s.record_id, s.vg_id, s.segment, "
                                          "s.mask, s.type, s.segment_id, v.uid, "
                                          "v.descr, v.login, v.agrm_id, v.blocked, "
                                          "v.tar_id, v.amount, v.block_date "
                                          "FROM staff s LEFT JOIN _vgroups_view v "
                                          "ON s.vg_id = v.vg_id "
                                          "WHERE s.segment LIKE {0} ;".format(str(dotted_ip_to_int(ip)))))

    def fetch_data_by_mac(self, mac):
        if len(mac) == 0:
            raise ValueError('mac can\'t be empty')

        return parse_data(self.fetch_data("SELECT s.record_id, s.vg_id, s.segment, "
                                          "s.mask, s.type, s.segment_id, v.uid, "
                                          "v.descr, v.login, v.agrm_id, v.blocked, "
                                          "v.tar_id, v.amount, v.block_date "
                                          "FROM staff s RIGHT JOIN  _vgroups_view v "
                                          "ON s.vg_id = v.vg_id "
                                          "WHERE v.login LIKE '{0}' ;".format(mac)))

    def get_ip(self, mac):
        if len(mac) == 0:
            raise ValueError('mac can\'t be empty')
        ip = self.fetch_data("SELECT segment "
                             "FROM staff s RIGHT JOIN  _vgroups_view v "
                             "ON s.vg_id = v.vg_id "
                             "WHERE v.login LIKE '{0}' ;".format(mac))
        if ip is None:
            return None
        return int_to_dotted_ip(int(ip[0]))

    def fetch_data(self, query):
        if len(query) == 0:
            raise ValueError("query can't be empty")

        conn = self.mysql.connect()
        cursor = conn.cursor()

        try:
            cursor.execute(query)
            return cursor.fetchone()
        except Exception as e:
            raise e
        finally:
            cursor.close()
            conn.close()


def parse_data(data):  # todo переделать этот изврат во что-то более адекватное, например орм
    if data is None: return None
    return {
        'record_id': data[0],
        'vg_id': data[1],
        'segment': int_to_dotted_ip(int(data[2])),
        'mask': int_to_dotted_ip(int(data[3])),
        'type': data[4],
        'segment_id': data[5],
        'uid': data[6],
        'descr': data[7],
        'login': data[8],
        'argm_id': data[9],
        'blocked': data[10],
        'tar_id': data[11],
        'amount': data[12],
        'block_date': data[13]
    }


def int_to_dotted_ip(int_ip):
    octet = ''
    for exp in [3, 2, 1, 0]:
        octet = octet + str(int(int_ip / (256 ** exp))) + "."
        int_ip = int_ip % (256 ** exp)
    return octet.rstrip('.')


def dotted_ip_to_int(dotted_ip):
    exp = 3
    int_ip = 0
    for quad in dotted_ip.split('.'):
        int_ip = int_ip + (int(quad) * (256 ** exp))
        exp = exp - 1
    return int_ip